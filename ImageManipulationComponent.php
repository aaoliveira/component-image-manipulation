<?php
/**
 * Componente do cake para manipulacao de imagem
 * @author Tayron Miranda <falecom@tayronmiranda.com.br>
 * @param string file
 * @return Nome do arquivo que foi TRUE ou retorna FALSE
 * @since 22/03/2012
 */
class ImageManipulationComponent extends Component {
	/**
	 * Armazena caminho completo ate o arquivo
	 * @value {array} request data file
	 */
	private $_image = NULL;
	/**
	 * Armazena caminho completo ate o arquivo
	 * @value {array} request data file
	 */
	private $_newImage = NULL;		
	/**
	 * Armazena mensagem de retorno das acoes dos metodos
	 * @value {string} mensagens
	 */
	private $_message = NULL;	
	/**
	 * Armazena caminho fisico de onde as imagens ficarao no sistema
	 * @value {string} mensagens
	 */
	private $_path = '../webroot/img/upload/';
	
	
	/**
	 * Metodo que recebe caminho completo ate a imagem, verifica se ela realmente 
	 * existe e seta ela na variavel
	 * @param: {string} Recebe caminho completo do arquivo
	 */
	private function setImage( $image )
	{
		if( is_file( $this->_path . $image ) ){
			$this->_image = $this->_path . $image;
			$this->setWritePermission();
		}else{
			$this->_message = 'Imagem inexistente';
		}
	}
	
	
	/**
	 * Metodo que retorna mensagem de retorno das acoes
	 * @return {string} mensagem
	 */
	public function getMessage()
	{
		return $this->_message;
	}
	
	
	/**
	 * Metodo que prapara a imagem para ser usada
	 * @param: {string} width recebe a largura da imagem
	 * @param: {string} height recebe a altura
	 * @return: {boolean} TRUE or FALSE
	 */
	public function inicialize( $image = NULL )
	{            
		if( $image != NULL ){
			$this->setImage( $image );                        
			$this->createImage();                        
		}else{
			$this->_message = 'Deve se passar o caminho fisico da imagem';
		}

		return $this;
	}
	
	
	/**
	 * Metodo que redimensiona a imagem
	 * @param: {string} width recebe a largura da imagem
	 * @param: {string} height recebe a altura
	 * @return: {boolean} TRUE or FALSE
	 */	
	public function resize( $width, $height )
	{
		 $dataResize = array( 'width' => $width, 'height' => $height );
		 $data		 = $this->getNewDimension( $dataResize );
		
		$newImage = imagecreatetruecolor( $data['width'], $data['height'] );
		imagecopyresized( $newImage, $this->_newImage, 0,0,0,0, $data['width'], $data['height'], ImageSX( $this->_newImage ), ImageSY( $this->_newImage ) ); 
		
		$this->_message = 'A imagem foi redimensionada';	

		if( !imagejpeg( $newImage, $this->_image ) ){
			$this->_message = 'Erro ao realizar o redimensionamento';
			return FALSE;
		} 
		
		$this->_message = 'Erro desconhecido';
		$this->finalize();
		return $this;
	}
	
	
	/**
	 * Metodo que pega a altera e largura da imagem
	 * @return: {array} largura e altura da imagem
	 */
	private function getDimension()
	{
		return array( 'width'  => imagesx( $this->_newImage ),
					  'height' => imagesy( $this->_newImage ) );
	}
	
	
	/**
	 * Metodo que gera nova dimensao para a imagem que sera movida para o servidor
	 * @param: {array} array com a altura e largura que a imagem devera ter
	 * @reurn: {array} array com a altura e largura que a imagem devera ter
	 */
	private function getNewDimension( $newDimension )
	{
		$dimension	= $this->getDimension();

		// Definindo altura da imagem
		$percentW		= $this->calculePercent( $newDimension['width'], $dimension['width'] ) ;		
		$percentW		= ( $newDimension['width'] < $dimension['width'] ) ? - $percentW : $percentW;

		$newDimension['height'] = ( $dimension['height'] * $percentW / 100 );
		$newDimension['height'] = ( $newDimension['height'] < 0 ) ? $newDimension['height'] * ( -1 ) : $newDimension['height'];		

		// Definindo largura da imagem
		$percentH		= $this->calculePercent( $newDimension['height'], $dimension['height'] ) ;		
		$percentH		= ( $newDimension['height'] < $dimension['height'] ) ? - $percentH : $percentH;

		$newDimension['width'] = ( $dimension['width'] * $percentH / 100 );
		$newDimension['width'] = ( $newDimension['width'] < 0 ) ? $newDimension['width'] * ( -1 ) : $newDimension['width'];			
		
		return $newDimension;
	}	
	
	
	/**
	 * Metodo que aplica regra de 3 para achar achar um valor
	 * @param: {int} _valorOriginal
	 * @param: {int} _constante, um valor de comparacao
	 * @param: (int) _valorCompraracao 
	 */
	private function calculePercent( $valor = 25, $total = 100)
	{
		return ( $valor / $total ) * 100; // Retorno em %
	}
	
	
	/**
	 * MEtodo que seta permissao de escrita a um arquivo
	 * @return {boolean} TRUE or FALSE
	 */
	private function setWritePermission()
	{
		if( !is_writable( $this->_image ) ){
			if( !chmod( $this->_image, 0777 ) ){
				$this->_message = 'O arquivo não existe';
				return FALSE;
			}
		}
		
		return TRUE;
	}

	
	/**
	 * Metodo que pega o formato da imagem
	 * @return {string} formato da imagem
	 */
	private function getFileType()
	{
		$type = explode( '.', $this->_image );
		return trim( strtolower( array_pop( $type ) ) );
	}
	
	
	/**
	 * Metodo que cria imagem para manipulacao
	 */
	private function createImage()
	{
		switch( $this->getFileType() ){
			case 'png' : 
				$this->_message = 'Imagem criada';
				$this->_newImage = imagecreatefrompng( $this->_image );
				break;
			case 'jpeg': 
				$this->_message = 'Imagem criada';
				$this->_newImage = imagecreatefromjpeg( $this->_image );
				break;			
			case 'jpg': 
				$this->_message = 'Imagem criada';
				$this->_newImage = imagecreatefromjpeg( $this->_image );
				break;			
			case 'gif': 
				$this->_message = 'Imagem criada';
				$this->_newImage = imagecreatefromgif( $this->_image );
				break;						
			default:
				$this->_message = 'Não foi possível criar a imagem, formato não aceito';
		}		
	}
	
	
	/**
	 * Metod que mostra a imagem criada
	 */
	public function viewImage()
	{
		if( $this->_newImage != NULL ){
			switch( $this->getFileType() ){
				case 'png' : 
					echo header( 'Content-Type: image/png' )
					   . imagepng( $this->_newImage );
					exit();
					break;
				case 'jpeg': 
					echo header( 'Content-Type: image/jpeg' )
					   . imagejpeg( $this->_newImage );
					exit();
					break;			
				case 'jpg': 
					echo header( 'Content-Type: image/jpeg' )
					   . imagejpeg( $this->_newImage );
					exit();
					break;			
				default:
					$this->_message = 'Formado da imagem invalida para visualizacao';
			}		
		}else{
			$this->_message = 'Não ha imagem para ser visualizada';
		}

	}
	
	
	/**
	 * Metodo que destroi a imagem criada
	 */
	private function finalize()
	{
		imagedestroy( $this->_newImage );
		$this->_newImage = NULL;
	}
}